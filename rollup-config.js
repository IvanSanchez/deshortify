// In a console, run:
// rollup -c rollup-config.js

// let deps = Object.keys(require('./package').dependencies).concat(['fs']);
let pkg = require('./package');

export default {
	input: pkg.module,
	output: {
		file: pkg.main,
		format: 'cjs',
		sourcemap: true,
	},
	plugins: [
        require('rollup-plugin-buble')(),
// 		require('rollup-plugin-commonjs')(),
// 		require('rollup-plugin-node-resolve')()
	],
// 	external: deps
};

