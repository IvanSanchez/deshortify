
# v0.4.0

* Skip GDPR interstitials

# v0.3.0 (2017-12-06)

* Non-ES6 build (rollup&buble)
* Stop skipping linkedin URLs

# v0.2.2 (2017-06-20)

* Bugfix for the schema-less URLs workaround

# v0.2.1 (2017-06-20)

* Work around schema-less URLs

# v0.2.0 (2017-06-19)

* Add linting
* Couple more parameter cleaners

# v0.1.2 (2017-05-15)

* Couple more parameter cleaners

# v0.1.1 (2017-05-12)

* Bugfix for race conditions: Cache the unresolved promises too
* Bugfix for infinite loops: Actually carry breadcrumbs around
* Bugfix for weird URLs: Assume duplicated URL query params are invalid and are to be removed

# v0.1.0 (2017-05-12)

* First initial version
* Basic cache (un-cleanable, always-growing)
* Basic whitelist imported from oysttyer-deshortify
* Only implements the HEAD method (does not handle javascript or iframe redirects)




