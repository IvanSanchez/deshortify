# Deshortify

NodeJS module which turns short URLs into long, meaningful, crap-less URLs

## Features

Deshortify will:

* Follow URLs, performing HTTP HEAD requests.
* Get rid of useless URL parameters.
* Roll back a URL redirect when it looks like a paywall or similar.

For example, it will turn `http://bit.ly/2qzPrcN` into `http://www.liberation.fr/direct/element/une-elue-du-personnel-de-whirlpool-candidate-suppleante-pour-en-marche_63786/?utm_campaign=Echobox&utm_medium=Social&utm_source=Twitter#link_time=1494593774`, **and then** into `http://www.liberation.fr/direct/element/une-elue-du-personnel-de-whirlpool-candidate-suppleante-pour-en-marche_63786/#link_time=1494593774`.

Deshortify will keep a cache of resolved URLs, so that known ones are not re-requested on the network.

## Why

Because I don't like short URLs. They don't convey information.

Because I don't like URLs with spammy URL parameters that take up space in my screen.

Because I feel that the existing de-shortening libraries for NodeJS (like [unshort](https://www.npmjs.com/package/unshort), or [url-unshort](https://www.npmjs.com/package/url-unshort)) are lacking.

## Usage

In NodeJS:

```js
// Import the library
import Deshortifier from 'deshortify'

// Create an instance
let deshortifier = new Deshortifier({ verbose: true });

// Call deshortify(), which returns a Promise for a URL.
deshortifier.deshortify('https://t.co/p0N0Zl8rx6').then(u=>console.log('Final URL is ' + u));
```

As a CLI tool:

```
$ npm install -g deshortify
$ deshortify https://t.co/wy9S5P0Cd2
```

Using deshortify as a CLI tool, use the `-v` flag to go verbose. This will display all the followed URLs, the cleanup performed, and the rollback (if any):

```
$ deshortify -v http://t.co/D01g3QPL7G
follow:  http://t.co/D01g3QPL7G  →  https://t.co/D01g3QPL7G
follow:  https://t.co/D01g3QPL7G  →  http://bit.ly/2F6pXtg
follow:  http://bit.ly/2F6pXtg  →  https://mydata2019.org/press-release-finland-leads-europe-towards-a-fair-data-economy/
https://mydata2019.org/press-release-finland-leads-europe-towards-a-fair-data-economy/
```

