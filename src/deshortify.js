const http = require("http");
const https = require("https");
const parseUrl = require("url").parse;
const resolveUrl = require("url").resolve;
const formatUrl = require("url").format;

export default class Deshortifier {
	// module.exports =  class Deshortifier {
	constructor(options = {}) {
		this._cache = {};
		this._verbose = !!options.verbose;

		if (process) {
			let deshortifyVersion = require("../package.json").version;
			let nodejsVersion = process.release.name + "/" + process.version;
			// Nice user agent for most URLs
			this._userAgent =
				options.userAgent ||
				"Deshortify/" +
					deshortifyVersion +
					" " +
					nodejsVersion +
					" (+https://gitlab.com/IvanSanchez/deshortify)";

			// User agent for asshole websites which filter requests based on it (I'm looking at you, facebook)
			this._assholeUserAgent =
				// 				"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/602.3.12 (KHTML, like Gecko) Version/10.0.2 Safari/602.3.12";
				"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) Gecko/20100101 Firefox/67.0";
		} else {
			// User agent when running on browser is the same as the navigator's
			this._userAgent = this._assholeUserAgent = window.navigator.userAgent;
		}
	}

	// Returns a Promise for the deshortified URL
	deshortify(url) {
		// Try parsing it, to catch URLs without schema.
		let parsedUrl = parseUrl(url);
		if (!parsedUrl.protocol) {
			url = "http://" + url;
		}

		return this._deshortify(url);
	}

	_deshortify(url, breadcrumbs = []) {
		if (
			breadcrumbs.indexOf(url) !== -1 || // Circular loop, break it.
			breadcrumbs.length > 20 || // This looks like an infinite non-circular loop, break it.
			this._skipUrl(url) // URL whitelisted from being deshortified because nuisances
		) {
			if (this._verbose) {
				console.log("Skipping: ", url);
			}
			return Promise.resolve(this._cleanUp(url));
		}

		breadcrumbs.push(url);

		if (url in this._cache) {
			let cachedUrl = this._cache[url];

			// Several requests for the same URL might happen before any of them resolves. If that's the case, just
			// return the promise already created.
			if (cachedUrl instanceof Promise) {
				return cachedUrl;
			}

			if (cachedUrl === url) {
				return Promise.resolve(this._cleanUp(url));
			}

			if (this._verbose) {
				console.log(
					"cached follow: ",
					url,
					" → ",
					cachedUrl,
					" (breadcrumbs lenght is ",
					breadcrumbs.length,
					")"
				);
			}
			return this._deshortify(cachedUrl, breadcrumbs);
		}

		let parsedUrl = parseUrl(url);

		if (parsedUrl.protocol !== "http:" && parsedUrl.protocol !== "https:") {
			// Neither HTTP or HTTPS, just return whatever (might be ftp, or something more esoteric as irc or gopher)
			return Promise.resolve(url);
		}

		// console.log(breadcrumbs);
		let headers =
			parsedUrl.hostname === "fb.me" ||
			parsedUrl.hostname.match(/\.app\.link$/)
				? {
						"Host": parsedUrl.hostname,
						"Accept-Encoding": "gzip, deflate, br",
						"User-Agent": this._assholeUserAgent,
						Accept:
							"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
						"Accept-Language": "en-US,en;q=0.5",
						DNT: 1,
						// Connection: "keep-alive",
						"Upgrade-Insecure-Requests": 1,
						Pragma: "no-cache",
						"Cache-Control": "no-cache",
				  }
				: { "User-Agent": this._userAgent };

		// Get referrer from breadcrumbs
		// 		if (breadcrumbs.length >= 2) {
		// 			headers.
		// 		}

		// Handle header-based redirects
		return (this._cache[url] = new Promise((resolve) => {
			let backend = parsedUrl.protocol === "http:" ? http : https;

			let request = backend.request(
				{
					method: "HEAD",
					protocol: parsedUrl.protocol,
					hostname: parsedUrl.hostname,
					port: parsedUrl.port,
					path: parsedUrl.path,
					headers: headers,
				},
				(res) => {
					// 					debugger;
					//console.log(request.getHeaders());
					//console.log(res.headers);

					if ("location" in res.headers) {
						// w00t! We've got a 30x response and a redirect!
						let newUrl = resolveUrl(url, res.headers.location);
						if (this._rollbackUrl(newUrl)) {
							if (this._verbose) {
								console.log("rolling back: ", url, " ← ", newUrl);
							}
							return resolve(this._cleanUp(url));
						}
						if (this._verbose && url !== newUrl) {
							console.log("follow: ", url, " → ", newUrl);
						}
						this._cache[url] = newUrl;
						return resolve(this._deshortify(newUrl, breadcrumbs));
					}

					// Giving up, looks like this was the final URL.
					return resolve(this._cleanUp(url));
				}
			);

			request.on("error", () => {
				// Panic and return the original url.
				return resolve(this._cleanUp(url));
			});

			request.end(); // Actually send the request
		}));
	}

	// Cleans up spammy query parameters and hash bits.
	/// TODO: Use instead: https://gitlab.com/KevinRoebert/ClearUrls/blob/master/data/data.json
	_cleanUp(url) {
		let parsedUrl = parseUrl(url, true);
		let host = parsedUrl.host;

		let cleanedParams = {};
		let params = Object.keys(parsedUrl.query);

		if (params) {
			// 			console.log(url);
			params.forEach((name) => {
				let val = parsedUrl.query[name];

				// 				console.log(host, ' / ', name, ' = ', val);
				// 				console.log('Matches _source: ', name.match(/_source$/));

				if (
					typeof val !== "string" || // e.g. http://www.businessinsider.com/...&r=US&IR=T&IR=T
					name.match(/_source$/) ||
					name.match(/_medium$/) ||
					name.match(/_term$/) ||
					name.match(/_content$/) ||
					name.match(/_campaign$/) ||
					name.match(/_mchannel/) ||
					name.match(/_kwd$/) ||
					name.match(/_brand$/) ||
					name.match(/_social-type$/) ||
					name === "utm_cid" ||
					name === "cm_mmc" ||
					(name === "tag" && val === "as.rss") ||
					(name === "ref" && val === "rss") ||
					(name === "ref" && val === "tw") ||
					(name === "ref" && val === "twtrec") ||
					(name === "ref" && val === "tw-share") ||
					(name === "ref" && val === "sharetw") ||
					(name === "id_externo_rsoc" && val === "TW_CM") ||
					(name === "newsfeed" && val === "true") ||
					(name === "spref" && val === "tw") ||
					(name === "spref" && val === "fb") ||
					(name === "spref" && val === "gr") ||
					val.match(/^twitter/) ||
					val.match(/\.twitter$/) ||
					val === "share_btn_tw" ||
					(name === "platform" && val === "hootsuite") ||
					(name === "mbid" && val === "social_retweet") || // New Yorker et al
					(name === "mbid" && val === "social_twitter") || // New Yorker et al
					(host === "www.youtube.com" && name === "feature") ||
					(host === "www.nytimes.com" && name === "smid") ||
					(host === "www.nytimes.com" && name === "seid") ||
					name === "awesm" || // Appears as a logger of awesm shortener, at least in storify
					(name === "CMP" && val === "twt_gu") || // Guardian.co.uk short links
					(name === "CMP" && val.match(/^soc_/)) || // Guardian.co.uk short links
					(name === "CMP" && val.match(/^Share_/)) ||
					(name === "ex_cid" && val === "story-twitter") ||
					(name === "xid" && val.match(/^soc_socialflow/)) ||
					(name === "ocid" && val === "socialflow_twitter") ||
					(name === "ocid" && val === "socialflow_facebook") ||
					(name.match(/_src$/) && val === "social-sh") ||
					(name === "soc_trk" && val === "tw") ||
					name === "hootPostID" ||
					(name === "a" && val === "socialmedia") || // Meetup
					(host.match(/medium.com$/) && name === "source") ||
					(host.match(/elpais.com$/) && name === "id_externo_rsoc") ||
					(host.match(/washingtonpost.com$$/) && name === "postshare") ||
					(host.match(/washingtonpost.com$$/) &&
						name === "ss_tw-bottom") ||
					val === "rss-default" ||
					name === "__twitter_impression" ||
					(name === "src" && val === "syn") ||
					name === "fbclid"
				) {
					// Noop
					return;
				} else {
					cleanedParams[name] = val;
				}
			});
		}

		// 		console.log('cleanup: ', JSON.stringify(parsedUrl.query), ' → ', JSON.stringify(cleanedParams) );

		// Replace query params and delete duplicated stuff
		parsedUrl.query = cleanedParams;
		delete parsedUrl.search;
		delete parsedUrl.href;
		delete parsedUrl.path;

		// 		console.log(parsedUrl);
		var cleanedUrl = formatUrl(parsedUrl);
		if (this._verbose && url !== cleanedUrl) {
			console.log("cleanup: ", url, " → ", cleanedUrl);
		}
		return cleanedUrl;
	}

	// Returns boolean true if the passed URL should be skipped altogether,
	// false otherwise
	_skipUrl(url) {
		let parsedUrl = parseUrl(url, true);

		let { host } = parsedUrl;

		return (
			url.length > 400 || // Just too long, maybe a REPL
			host === "youtu.be" || // Does not add more info
			host === "spoti.fi" || // Does not add more info
			host === "4sq.com" || // Does not add more info
			host === "flic.kr" || // Does not add more info
			host === "untp.beer" || // Does not add more info
			host === "youtube.com" || // Does not add more info
			host === "www.elmundo.es" || // El Mundo newspaper will only timeout and waste time
			host === "www.economist.com" || // "You are banned from this site.  Please contact via a different client configuration if you believe that this is a mistake."
			host === "pbs.twimg.com" || // Might trigger verbose errors if twitter is over capacity
			// 			host === "www.linkedin.com"      ||	// Used to redirect to login
			host === "session.wikispaces.com" || // Infinite redirect loop with different URLs params each time
			host.match(/twitter\.com$/) || // Infinite redirect to login
			host.match(/blogspot\.[a-z]{2-3}$/) || // Will redirect to a nearby geolocated server
			host.match(/facebook\.com$/) || // Will redirect to fb.com/unsupportedbrowser due to user-agent
			host.match(/\.nytimes\.com$/) || // Infinite nocookies loop
			host.match(/^www\.amazon\.$/) // 405 MethodNotAllowed
		);
	}

	// Returns boolean true if the URL should be rolled back to the previous one,
	// false otherwise.
	// The goal is to avoid displaying URLs with obvious signs of being GPDR or
	// "no cookies" interstitials.
	_rollbackUrl(url) {
		let parsedUrl = parseUrl(url, true);

		let { host, pathname } = parsedUrl;

		return (
			url.match(/\/authorize/) || // Potential paywall (e.g. nature.com)
			url.match(/subscribe/) || // Potential paywall
			url.match(/nocookie/) || // Potential paywall/login
			url.match(/cookieAbsent/) || // Potential paywall/login
			url.match(/gdpr/) || // Potential "GDPR consent" interstitial
			url.match(/\/users\/sign_in/) || // Login page, e.g. gitlab
			(host === "www.bloomberg.com" && pathname.match(/^\/tosv2.html/)) // GPDR interstitial
		);
	}
}
