#!/usr/bin/env node

let Deshortifier = require('../dist/deshortify.js');

if (process.argv.length <= 2) {
	// Display usage
	console.log("Usage:\n" +
		" deshortify (url)\n" +
		" deshortify -v (url)\n" +
		"Deshortifies the given URL, following HTTP redirects and then cutting down " +
		"on useless URL parameters. Use the -v parameter to enable verbose mode."
	);
	sys.exit(1);
}

var d;
if (process.argv[2] == '-v') {
	let deshortifier = new Deshortifier({ verbose: true });
	d = deshortifier.deshortify(process.argv[3]);
} else {
	let deshortifier = new Deshortifier({ verbose: false });
	d = deshortifier.deshortify(process.argv[2]);
}

d.then(function(finalUrl) {
	console.log(finalUrl);
});

